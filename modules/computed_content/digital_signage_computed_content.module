<?php /** @noinspection PhpUnused */

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Render\Element;
use Drupal\digital_signage_computed_content\ComputedContentInterface;
use Drupal\digital_signage_computed_content\Entity\ComputedContentType;

/**
 * Implements hook_entity_extra_field_info().
 */
function digital_signage_computed_content_entity_extra_field_info() {
  $extra = [];
  foreach (ComputedContentType::loadMultiple() as $bundle) {
    $extra['digsig_computed_content'][$bundle->id()]['display'] = [
      'computed_content' => [
        'label' => t('Computed content'),
        'weight' => 0,
        'visible' => TRUE,
      ],
    ];
  }
  return $extra;
}

/**
 * Implements hook_ENTITY_TYPE_view().
 *
 * @param array $build
 * @param \Drupal\Core\Entity\EntityInterface $entity
 * @param \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display
 */
function digital_signage_computed_content_digsig_computed_content_view(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display) {
  if ($entity instanceof ComputedContentInterface && $display->getComponent('computed_content')) {
    /** @var \Drupal\digital_signage_computed_content\RenderInterface $service */
    $service = Drupal::service('digital_signage_computed_content.render.' . $entity->bundle());
    $build['computed_content'] = $service->getMarkup($entity);
  }
}

/**
 * Implements hook_theme().
 */
function digital_signage_computed_content_theme() {
  return [
    'digsig_computed_content' => [
      'render element' => 'elements',
    ],
  ];
}

/**
 * Prepares variables for digsig_computed_content templates.
 *
 * Default template: digital-signage-computed-content.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the digsig_computed_content information and any
 *     fields attached to the entity.
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_digsig_computed_content(array &$variables) {
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
