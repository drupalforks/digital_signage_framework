<?php

use Drupal\Core\Render\Element;

/**
 * Implements hook_cron().
 */
function digital_signage_device_cron() {
  if (Drupal::config('digital_signage_framework.settings')->get('cron_sync_devices')) {
    Drupal::service('plugin.manager.digital_signage_platform')->syncDevices();
  }
}

/**
 * Implements hook_theme().
 */
function digital_signage_device_theme() {
  return [
    'digital_signage_device' => [
      'render element' => 'elements',
    ],
  ];
}

/**
 * Prepares variables for device templates.
 *
 * Default template: digital-signage-device.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the device information and any
 *     fields attached to the entity.
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_digital_signage_device(array &$variables) {
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
