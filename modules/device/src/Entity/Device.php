<?php

namespace Drupal\digital_signage_device\Entity;

use Drupal;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Url;
use Drupal\digital_signage_device\DeviceInterface;
use Drupal\digital_signage_platform\PlatformInterface;
use Drupal\taxonomy\Entity\Term;

/**
 * Defines the device entity class.
 *
 * @ContentEntityType(
 *   id = "digital_signage_device",
 *   label = @Translation("Digital signage device"),
 *   label_collection = @Translation("Digital signage devices"),
 *   bundle_label = @Translation("Device type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\digital_signage_device\DeviceListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\digital_signage_device\DeviceAccessControlHandler",
 *     "form" = {
 *       "edit" = "Drupal\digital_signage_device\Form\Device",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "digital_signage_device",
 *   data_table = "digital_signage_device_field_data",
 *   revision_table = "digital_signage_device_revision",
 *   revision_data_table = "digital_signage_device_field_revision",
 *   show_revision_ui = TRUE,
 *   translatable = TRUE,
 *   admin_permission = "administer digital signage device types",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "langcode" = "langcode",
 *     "bundle" = "bundle",
 *     "label" = "title",
 *     "uuid" = "uuid"
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log"
 *   },
 *   links = {
 *     "canonical" = "/digital_signage_device/{digital_signage_device}",
 *     "edit-form" =
 *   "/admin/content/digital-signage-device/{digital_signage_device}/edit",
 *     "collection" = "/admin/content/digital-signage-device"
 *   },
 *   bundle_entity_type = "digital_signage_device_type",
 *   field_ui_base_route = "entity.digital_signage_device_type.edit_form"
 * )
 */
class Device extends RevisionableContentEntityBase implements DeviceInterface {

  use EntityChangedTrait;

  private function getTempScheduleStoreId() {
    return 'temp-schedule-store-' . $this->id();
  }

  /**
   * {@inheritdoc}
   */
  public function getPlugin(): PlatformInterface {
    return Drupal::service('plugin.manager.digital_signage_platform')->createInstance($this->bundle());
  }

  /**
   * {@inheritdoc}
   */
  public function getApiSpec($debug = FALSE, $reload_assets = FALSE, $reload_content = FALSE): array {
    return [
      'api' => Url::fromRoute('digital_signage_framework.api', [], [
        'absolute' => TRUE,
      ])->toString(),
      'deviceId' => $this->id(),
      'debug' => $debug,
      'reloadassets' => $reload_assets,
      'reloadcontent' => $reload_content,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function extId(): string {
    return $this->get('extid')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle(): string {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title): DeviceInterface {
    $this->set('title', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled(): bool {
    return (bool) $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus($status): DeviceInterface {
    $this->set('status', $status);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function needsScheduleUpdate(): bool {
    return (bool) $this->get('needs_update')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function scheduleUpdate(): DeviceInterface {
    $this->getPlugin()->deleteRecord($this->getTempScheduleStoreId());
    $this->set('needs_update', TRUE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function scheduleUpdateCompleted(): DeviceInterface {
    $this->set('needs_update', FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime(): int {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function addSegment($segment): bool {
    /** @var Term[] $segmentEntities */
    $segmentEntities = taxonomy_term_load_multiple_by_name($segment, 'digital_signage');
    if (empty($segmentEntities)) {
      $term = Term::create([
        'vid' => 'digital_signage',
        'name' => $segment,
      ]);
      $term->save();
      $segmentEntities = [$term];
    }

    $changed = FALSE;
    /** @var Term[] $existingSegments */
    $existingSegments = $this->get('segments')->referencedEntities();
    $existingSegmentIds = $this->getSegmentIds();
    foreach ($segmentEntities as $segmentEntity) {
      if (!in_array($segmentEntity->id(), $existingSegmentIds, TRUE)) {
        $existingSegments[] = $segmentEntity;
        $changed = TRUE;
      }
    }
    if ($changed) {
      $this->set('segments', $existingSegments);
    }
    return $changed;
  }

  /**
   * {@inheritdoc}
   */
  public function getSegmentIds(): array {
    $ids = [];
    /** @var \Drupal\taxonomy\TermInterface $entity */
    foreach ($this->get('segments')->referencedEntities() as $entity) {
      $ids[] = $entity->id();
    }
    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  public function getSchedule($stored = TRUE) {
    if ($stored) {
      return $this->get('schedule')->referencedEntities()[0] ?? NULL;
    }
    $plugin = $this->getPlugin();
    $id = $this->getTempScheduleStoreId();
    $schedule = $plugin->getRecord($id);
    if ($schedule === NULL) {
      $schedule = Drupal::service('schedule.manager.digital_signage_platform')->getSchedule($this);
      $plugin->storeRecord($id, $schedule);
    }
    return $schedule;
  }

  /**
   * {@inheritdoc}
   */
  public function setSchedule($schedule): DeviceInterface {
    $this->set('schedule', $schedule);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getWidth(): int {
    return $this->get('width')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getHeight(): int {
    return $this->get('height')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getOrientation(): string {
    return $this->getHeight() > $this->getWidth() ?
      'portrait' :
      'landscape';
  }

  /**
   * {@inheritdoc}
   */
  public function hasPinLocation(): bool {
    return !($this->get('pin_location_x')->value === NULL || $this->get('pin_location_y')->value === NULL);
  }

  /**
   * {@inheritdoc}
   */
  public function getPinLocation(): array {
    return [
      'x' => $this->get('pin_location_x')->value ?? 0,
      'y' => $this->get('pin_location_y')->value ?? 0,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['extid'] = BaseFieldDefinition::create('string')
      ->setRevisionable(FALSE)
      ->setTranslatable(FALSE)
      ->setLabel(t('External ID'))
      ->setDescription(t('The external ID of the device entity.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel(t('Title'))
      ->setDescription(t('The title of the device entity.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setRevisionable(TRUE)
      ->setLabel(t('Status'))
      ->setDescription(t('A boolean indicating whether the device is enabled.'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enabled')
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'above',
        'weight' => 1,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['needs_update'] = BaseFieldDefinition::create('boolean')
      ->setRevisionable(TRUE)
      ->setLabel(t('Needs update'))
      ->setDescription(t('A boolean indicating whether the device needs a schedule update.'))
      ->setDefaultValue(FALSE)
      ->setSetting('on_label', 'Yes')
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'above',
        'weight' => 2,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['description'] = BaseFieldDefinition::create('text_long')
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel(t('Description'))
      ->setDescription(t('A description of the device.'))
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => 3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'label' => 'above',
        'weight' => 3,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setTranslatable(TRUE)
      ->setDescription(t('The time that the device was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 4,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setTranslatable(TRUE)
      ->setDescription(t('The time that the device was last edited.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['segments'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Segments'))
      ->setSetting('target_type', 'taxonomy_term')
      ->setSetting('handler_settings', ['target_bundles' => ['digital_signage' => 'digital_signage']])
      ->setRequired(FALSE)
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ],
        'weight' => 10,
      ])
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_label',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['schedule'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Schedule'))
      ->setSetting('target_type', 'digital_signage_schedule')
      ->setRequired(FALSE)
      ->setCardinality(1)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'digital_signage_schedule_preview',
        'weight' => 2,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['width'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Width'))
      ->setRequired(TRUE)
      ->setDefaultValue(1920)
      ->setCardinality(1)
      ->setDisplayOptions('form', [
        'settings' => [
          'size' => 'normal',
          'unsigned' => TRUE,
          'min' => 10,
        ],
        'weight' => 6,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'weight' => 6,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['height'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Height'))
      ->setRequired(TRUE)
      ->setDefaultValue(1080)
      ->setCardinality(1)
      ->setDisplayOptions('form', [
        'settings' => [
          'size' => 'normal',
          'unsigned' => TRUE,
          'min' => 10,
        ],
        'weight' => 7,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'weight' => 7,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['geolocation'] = BaseFieldDefinition::create('geolocation')
      ->setLabel(t('Geo location'))
      ->setRequired(FALSE)
      ->setCardinality(1)
      ->setDisplayOptions('form', [
        'weight' => 8,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'weight' => 8,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['perspective'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Perspective'))
      ->setRequired(FALSE)
      ->setCardinality(1)
      ->setDisplayOptions('form', [
        'settings' => [
          'size' => 'normal',
          'unsigned' => TRUE,
          'min' => 0,
          'max' => 360,
        ],
        'weight' => 9,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'weight' => 9,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['pin_location_x'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('PIN location x'))
      ->setRequired(FALSE)
      ->setCardinality(1)
      ->setDisplayOptions('form', [
        'settings' => [
          'size' => 'normal',
          'unsigned' => TRUE,
          'min' => 0,
        ],
        'weight' => 9,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'weight' => 9,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
    $fields['pin_location_y'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('PIN location y'))
      ->setRequired(FALSE)
      ->setCardinality(1)
      ->setDisplayOptions('form', [
        'settings' => [
          'size' => 'normal',
          'unsigned' => TRUE,
          'min' => 0,
        ],
        'weight' => 9,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'weight' => 9,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
