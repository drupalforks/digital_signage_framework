(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.digital_signage_preview = Drupal.digital_signage_preview || {};
  drupalSettings.digital_signage = drupalSettings.digital_signage || {};
  drupalSettings.digital_signage.devices = drupalSettings.digital_signage.devices || [];
  drupalSettings.digital_signage.preview = false;
  drupalSettings.digital_signage.diagram = false;
  drupalSettings.digital_signage.screenshot = false;
  drupalSettings.digital_signage.log = false;

  Drupal.behaviors.digital_signage_preview = {
    attach: function () {
      $('body:not(.digital-signage-preview-processed)')
        .addClass('digital-signage-preview-processed')
        .each(function () {
          $('body').append('<div id="digital-signage-preview"><div class="background">&nbsp;</div><div class="popup"><div class="content">&nbsp;</div></div></div>');
        });
      $('#digital-signage-preview .background:not(.digital-signage-preview-processed)')
        .addClass('digital-signage-preview-processed')
        .on('click', function () {
          Drupal.digital_signage_preview.stop(true, true, true, true);
          return false;
        });

      // Schedule button
      $('input.button.digital-signage.preview:not(.digital-signage-preview-processed)')
        .addClass('digital-signage-preview-processed')
        .on('click', function () {
          Drupal.digital_signage_preview.start($(this).attr('device-id'), $(this).attr('stored-schedule'));
          return false;
        });

      // Diagram button
      $('input.button.digital-signage.diagram:not(.digital-signage-preview-processed)')
        .addClass('digital-signage-preview-processed')
        .on('click', function () {
          Drupal.digital_signage_preview.showDiagram($(this).attr('device-id'), $(this).attr('stored-schedule'));
          return false;
        });

      // Screenshot button
      $('input.button.digital-signage.screenshot:not(.digital-signage-preview-processed)')
        .addClass('digital-signage-preview-processed')
        .on('click', function () {
          Drupal.digital_signage_preview.showScreenshot($(this).attr('device-id'), 'true', false);
          return false;
        });
      $('#digital-signage-preview.show .popup > .content .screenshot-widget:not(.digital-signage-preview-processed)')
        .addClass('digital-signage-preview-processed')
        .on('click', function () {
          Drupal.digital_signage_preview.showScreenshot($('input.button.digital-signage.screenshot').attr('device-id'), 'true', true);
          return false;
        });

      // Log buttons
      $('input.button.digital-signage.debug-log:not(.digital-signage-preview-processed)')
        .addClass('digital-signage-preview-processed')
        .on('click', function () {
          Drupal.digital_signage_preview.showLog($(this).attr('device-id'), 'true', 'debug');
          return false;
        });
      $('input.button.digital-signage.error-log:not(.digital-signage-preview-processed)')
        .addClass('digital-signage-preview-processed')
        .on('click', function () {
          Drupal.digital_signage_preview.showLog($(this).attr('device-id'), 'true', 'error');
          return false;
        });
    }
  };

  Drupal.digital_signage_preview.showDiagram = function ($deviceId, $storedSchedule) {
    drupalSettings.digital_signage.diagram = true;
    $('#digital-signage-preview').addClass('show');
    Drupal.digital_signage_preview.setPreviewSize(0);
    let $settings = drupalSettings.digital_signage.devices[$deviceId];
    Drupal.ajax({
      url: $settings.schedule.api + '?storedSchedule=' + $storedSchedule + '&deviceId=' + $deviceId + '&mode=diagram',
      error: function (e) {
        console.log(e);
        Drupal.digital_signage_preview.stop(false, true, false, false);
      }
    }).execute();
    $(document).keydown(function (e) {
      if (e.keyCode === 27) {
        Drupal.digital_signage_preview.stop(false, true, false, false);
      }
    });
  };

  Drupal.digital_signage_preview.showLog = function ($deviceId, $storedSchedule, $type) {
    drupalSettings.digital_signage.log = true;
    $('#digital-signage-preview').addClass('show');
    Drupal.digital_signage_preview.setPreviewSize(0);
    let $settings = drupalSettings.digital_signage.devices[$deviceId];
    Drupal.ajax({
      url: $settings.schedule.api + '?storedSchedule=' + $storedSchedule + '&deviceId=' + $deviceId + '&mode=log&type=' + $type,
      error: function (e) {
        console.log(e);
        Drupal.digital_signage_preview.stop(false, false, false, true);
      }
    }).execute();
    $(document).keydown(function (e) {
      if (e.keyCode === 27) {
        Drupal.digital_signage_preview.stop(false, false, false, true);
      }
    });
  };

  Drupal.digital_signage_preview.showScreenshot = function ($deviceId, $storedSchedule, $refresh) {
    let $settings = drupalSettings.digital_signage.devices[$deviceId];
    if (!$refresh) {
      drupalSettings.digital_signage.screenshot = true;
      $('#digital-signage-preview')
        .addClass('orientation-' + $settings.orientation)
        .addClass('show');
      Drupal.digital_signage_preview.setPreviewSize($settings.proportion);
      $(document).keydown(function (e) {
        if (e.keyCode === 27) {
          Drupal.digital_signage_preview.stop(false, false, true, false);
        }
      });
    }
    Drupal.ajax({
      url: $settings.schedule.api + '?storedSchedule=' + $storedSchedule + '&deviceId=' + $deviceId + '&mode=screenshot&refresh=' + $refresh,
      error: function (e) {
        console.log(e);
        Drupal.digital_signage_preview.stop(false, false, true, false);
      }
    }).execute();
  };

  Drupal.digital_signage_preview.start = function ($deviceId, $storedSchedule) {
    drupalSettings.digital_signage.preview = true;
    drupalSettings.digital_signage.automatic = true;
    drupalSettings.digital_signage.active = {};
    drupalSettings.digital_signage.active.settings = drupalSettings.digital_signage.devices[$deviceId];
    drupalSettings.digital_signage.active.index = 0;
    drupalSettings.digital_signage.active.device = $deviceId;
    $('#digital-signage-preview')
      .addClass('orientation-' + drupalSettings.digital_signage.active.settings.orientation)
      .addClass('show');
    Drupal.digital_signage_preview.setPreviewSize(drupalSettings.digital_signage.active.settings.proportion);
    $(document).keydown(function (e) {
      if (e.keyCode === 27) {
        Drupal.digital_signage_preview.stop(true, false, false, false);
      }
      if (e.keyCode === 37) {
        // Show previous slide.
        drupalSettings.digital_signage.automatic = false;
        drupalSettings.digital_signage.active.index -= 2;
        Drupal.digital_signage_preview.load($storedSchedule);
      }
      if (e.keyCode === 39) {
        // Show next slide.
        drupalSettings.digital_signage.automatic = false;
        Drupal.digital_signage_preview.load($storedSchedule);
      }
    });
    Drupal.ajax({
      url: drupalSettings.digital_signage.active.settings.schedule.api + '?storedSchedule=' + $storedSchedule + '&deviceId=' + $deviceId + '&mode=schedule',
      error: function (e) {
        if (e.status === 200) {
          drupalSettings.digital_signage.active.settings.schedule.items = e.responseJSON;
          Drupal.digital_signage_preview.load($storedSchedule);
        }
        else {
          console.log(e);
          Drupal.digital_signage_preview.stop(true, false, false, false);
        }
      }
    }).execute();
  };

  Drupal.digital_signage_preview.load = function ($storedSchedule) {
    if (drupalSettings.digital_signage.active.index + 1 > drupalSettings.digital_signage.active.settings.schedule.items.length) {
      drupalSettings.digital_signage.active.index = 0;
    }
    if (drupalSettings.digital_signage.active.index < 0) {
      drupalSettings.digital_signage.active.index = drupalSettings.digital_signage.active.settings.schedule.items.length - 1;
    }
    let $item = drupalSettings.digital_signage.active.settings.schedule.items[drupalSettings.digital_signage.active.index];
    let $timeout = $item.duration * 1000;
    drupalSettings.digital_signage.active.index++;
    if (drupalSettings.digital_signage.preview) {
      // Check if new item is different from current item.
      if (drupalSettings.digital_signage.active.currentitem === undefined ||
          drupalSettings.digital_signage.active.currentitem.type !== $item.type ||
          drupalSettings.digital_signage.active.currentitem.entity_type !== $item.entity.type ||
          drupalSettings.digital_signage.active.currentitem.entity_id !== $item.entity.id) {
        // Remember new item as current item.
        drupalSettings.digital_signage.active.currentitem = {
          type: $item.type,
          entity_type: $item.entity.type,
          entity_id: $item.entity.id,
        };
        // Load new item.
        Drupal.ajax({
          url: drupalSettings.digital_signage.active.settings.schedule.api + '?storedSchedule=' + $storedSchedule + '&deviceId=' + drupalSettings.digital_signage.active.device + '&mode=preview&type=' + $item.type + '&entityType=' + $item.entity.type + '&entityId=' + $item.entity.id,
          error: function (e) {
            console.log(e);
            Drupal.digital_signage_preview.stop(true, false, false, false);
          }
        }).execute();
      }
      setTimeout(function () {
        if (drupalSettings.digital_signage.automatic) {
          Drupal.digital_signage_preview.load($storedSchedule);
        }
      }, $timeout);
    }
  };

  Drupal.digital_signage_preview.stop = function ($preview, $diagram, $screenshot, $log) {
    if ($preview) {
      drupalSettings.digital_signage.preview = false;
    }
    if ($diagram) {
      drupalSettings.digital_signage.diagram = false;
    }
    if ($screenshot) {
      drupalSettings.digital_signage.screenshot = false;
    }
    if ($log) {
      drupalSettings.digital_signage.log = false;
    }
    if (!drupalSettings.digital_signage.preview && !drupalSettings.digital_signage.diagram && !drupalSettings.digital_signage.screenshot && !drupalSettings.digital_signage.log) {
      $('#digital-signage-preview')
        .removeClass('orientation-landscape')
        .removeClass('orientation-portrait')
        .removeClass('show');
      $('#digital-signage-preview .popup > .content').html('');
    }
  };

  Drupal.digital_signage_preview.setPreviewSize = function ($proportion) {
    let viewport = $('#digital-signage-preview .background');
    let screenH = viewport.height();
    let screenW = viewport.width();
    let gapLeftRight = '10%';
    let width = '10%';
    let height = '10%';
    let gapTopBottom = '10%';
    if ($proportion > 1) {
      gapLeftRight = screenW / 10;
      width = screenW - 2 * gapLeftRight;
      height = width / $proportion;
      gapTopBottom = (screenH - height) / 2;
    }
    else if ($proportion > 0) {
      gapTopBottom = screenH / 10;
      height = screenH - 2 * gapTopBottom;
      width = height * $proportion;
      gapLeftRight = (screenW - width) / 2;
    }
    gapLeftRight -= 15;
    gapTopBottom -= 15;
    $('#digital-signage-preview .popup')
      .css('top', gapTopBottom)
      .css('bottom', gapTopBottom)
      .css('left', gapLeftRight)
      .css('right', gapLeftRight);
  };

})(jQuery, Drupal, drupalSettings);
