<?php

namespace Drupal\digital_signage_schedule\Plugin\Action;

use Drupal\Core\Access\AccessResultAllowed;
use Drupal\Core\Action\ConfigurableActionBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\digital_signage_schedule\ScheduleManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Reload schedules and configuration on devices.
 *
 * @Action(
 *   id = "digital_signage_schedule_reload",
 *   label = @Translation("Reload schedules and configuration"),
 *   type = "digital_signage_device"
 * )
 */
class ReloadSchedules extends ConfigurableActionBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\digital_signage_schedule\ScheduleManager
   */
  protected $scheduleManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ScheduleManager $schedule_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->scheduleManager = $schedule_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('schedule.manager.digital_signage_platform')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    return AccessResultAllowed::allowedIf($account !== NULL && $account->hasPermission('reload digital signage schedule'));
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'debugmode' => FALSE,
      'reloadassets' => FALSE,
      'reloadcontent' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['debugmode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Debug mode'),
      '#default_value' => $this->configuration['debugmode'],
    ];
    $form['reloadassets'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Reload assets (CSS, JS, fonts) on each schedule restart'),
      '#default_value' => $this->configuration['reloadassets'],
    ];
    $form['reloadcontent'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Reload content on each schedule restart'),
      '#default_value' => $this->configuration['reloadcontent'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['debugmode'] = $form_state->getValue('debugmode');
    $this->configuration['reloadassets'] = $form_state->getValue('reloadassets');
    $this->configuration['reloadcontent'] = $form_state->getValue('reloadcontent');
  }

  /**
   * {@inheritdoc}
   */
  public function execute($device = NULL) {
    $this->scheduleManager->reloadSchedules(
      $device->id(),
      $this->configuration['debugmode'],
      $this->configuration['reloadassets'],
      $this->configuration['reloadcontent']
    );
  }

}
