<?php

namespace Drupal\digital_signage_schedule;

/**
 * Interface for digital_signage_schedule_generator plugins.
 */
interface ScheduleGeneratorInterface {

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label(): string;

  /**
   * Generates the actual schedule.
   *
   * @param \Drupal\digital_signage_device\DeviceInterface $device
   * @param \Drupal\digital_signage_content_setting\Entity\ContentSetting[] $contentSettings
   *
   * @return \Drupal\digital_signage_schedule\SequenceItem[]
   */
  public function generate($device, $contentSettings): array;

}
