<?php

namespace Drupal\digital_signage_schedule;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\digital_signage_device\DeviceInterface;
use Drupal\digital_signage_platform\PlatformPluginManager;
use Drupal\digital_signage_schedule\Entity\Schedule;

class ScheduleManager {

  /**
   * @var \Drupal\digital_signage_schedule\ScheduleGeneratorPluginManager
   */
  protected $generatorPluginManager;

  /**
   * @var \Drupal\digital_signage_platform\PlatformPluginManager
   */
  protected $platformPluginManager;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  public function __construct(ScheduleGeneratorPluginManager $generator_plugin_manager, PlatformPluginManager $platform_plugin_manager, EntityTypeManager $entity_type_manager, ModuleHandlerInterface $module_handler) {
    $this->generatorPluginManager = $generator_plugin_manager;
    $this->platformPluginManager = $platform_plugin_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->moduleHandler = $module_handler;
  }

  /**
   * @param int|null $deviceId
   * @param bool $all
   *
   * @return \Drupal\digital_signage_device\DeviceInterface[]
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getDevices($deviceId = NULL, $all = FALSE): array {
    $deviceManager = $this->entityTypeManager->getStorage('digital_signage_device');
    $query = $deviceManager->getQuery();
    if ($deviceId !== NULL) {
      $query->condition('id', $deviceId);
    }
    if (!$all) {
      $query->condition('needs_update', 1);
    }
    $ids = $query
      ->execute();
    return $deviceManager->loadMultiple($ids);
  }

  /**
   * @param \Drupal\digital_signage_device\DeviceInterface $device
   * @param \Drupal\digital_signage_schedule\ScheduleGeneratorInterface $plugin
   * @param bool $save_and_push
   * @param bool $force
   * @param bool $debug
   * @param bool $reload_assets
   * @param bool $reload_content
   * @param string|null $entityType
   * @param int|null $entityId
   *
   * @return \Drupal\digital_signage_schedule\ScheduleInterface
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function pushSchedule(DeviceInterface $device, ScheduleGeneratorInterface $plugin, $save_and_push, $force = FALSE, $debug = FALSE, $reload_assets = FALSE, $reload_content = FALSE, $entityType = NULL, $entityId = NULL) {
    // Collect the content entities for this schedule
    $query = $this->entityTypeManager
      ->getStorage('digital_signage_content_setting')
      ->getQuery()
      ->condition('status', 1);
    if ($entityType === NULL) {
      $orQuery = $query->orConditionGroup()
        ->condition($query->andConditionGroup()
          ->notExists('devices.target_id')
          ->notExists('segments.target_id')
        )
        ->condition('devices.target_id', $device->id());
      if ($segmentIds = $device->getSegmentIds()) {
        $orQuery->condition('segments.target_id', $segmentIds, 'IN');
      }
      $query->condition($orQuery);

      // Allow other modules to alter the query for entities.
      $this->moduleHandler->alter('digital_signage_schedule_generator_query', $query, $device);
    }
    else {
      $query
        ->condition('parent_entity__target_type', $entityType)
        ->condition('parent_entity__target_id', $entityId);
    }

    $contentSettings = [];
    $hashMap = [];
    /** @var \Drupal\digital_signage_content_setting\ContentSettingInterface $item */
    foreach ($this->entityTypeManager->getStorage('digital_signage_content_setting')->loadMultiple($query->execute()) as $item) {
      /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
      if (($entity = $this->entityTypeManager->getStorage($item->getReverseEntityType())->load($item->getReverseEntityId())) &&
        (!$entity->hasField('status') || $entity->get('status')->value)) {
        $contentSettings[] = $item;
        $hash_item = [
          'type' => $entity->getEntityTypeId(),
          'id' => $entity->id(),
        ];
        if ($entity->hasField('changed')) {
          $hash_item['changed'] = $entity->get('changed')->value;
        }
        else if ($entity->hasField('created')) {
          $hash_item['created'] = $entity->get('created')->value;
        }
      $hashMap[] = $hash_item;
      }
    }

    // See if we already have a schedule with that.
    $scheduleHash = md5(json_encode($hashMap));
    try {
      /** @var \Drupal\digital_signage_schedule\ScheduleInterface[] $schedules */
      $schedules = $this->entityTypeManager
        ->getStorage('digital_signage_schedule')
        ->loadByProperties([
          'hash' => $scheduleHash,
        ]);
    } catch (InvalidPluginDefinitionException $e) {
    } catch (PluginNotFoundException $e) {
    }
    if ($force || empty($schedules)) {
      // We don't have one yet, let's create a new one.
      $items = [];
      foreach ($plugin->generate($device, $contentSettings) as $item) {
        $items[] = $item->toArray();
      }
      /** @var \Drupal\digital_signage_schedule\ScheduleInterface $schedule */
      $schedule = Schedule::create([
        'hash' => $scheduleHash,
        'items' => [$items],
      ]);
      if ($save_and_push) {
        $schedule->save();
      }
    }
    else {
      $schedule = reset($schedules);
    }

    // Store and push the schedule with the device if necessary.
    if ($save_and_push) {
      if ($force || !$device->getSchedule() || $device->getSchedule()->id() !== $schedule->id()) {
        $device->setSchedule($schedule);
        $this->platformPluginManager->pushSchedule($device, $debug, $reload_assets, $reload_content);
      }
      $device
        ->scheduleUpdateCompleted()
        ->save();
    }

    return $schedule;
  }

  /**
   * @param int|null $deviceId
   * @param bool $force
   * @param bool $debug
   * @param bool $reload_assets
   * @param bool $reload_content
   * @param string|null $entityType
   * @param int|null $entityId
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function pushSchedules($deviceId = NULL, $force = FALSE, $debug = FALSE, $reload_assets = FALSE, $reload_content = FALSE, $entityType = NULL, $entityId = NULL) {
    /** @var \Drupal\digital_signage_schedule\ScheduleGeneratorInterface $plugin */
    $plugin = $this->generatorPluginManager->createInstance('default');
    foreach ($this->getDevices($deviceId, $force) as $device) {
      $this->pushSchedule($device, $plugin, TRUE, $force, $debug, $reload_assets, $reload_content, $entityType, $entityId);
    }
  }

  /**
   * @param null $deviceId
   * @param false $debug
   * @param false $reload_assets
   * @param false $reload_content
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function reloadSchedules($deviceId = NULL, $debug = FALSE, $reload_assets = FALSE, $reload_content = FALSE) {
    foreach ($this->getDevices($deviceId, TRUE) as $device) {
      $this->platformPluginManager->reloadSchedule($device, $debug, $reload_assets, $reload_content);
    }
  }

  /**
   * @param \Drupal\digital_signage_device\DeviceInterface $device
   *
   * @return \Drupal\digital_signage_schedule\ScheduleInterface
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function getSchedule(DeviceInterface $device) {
    /** @var \Drupal\digital_signage_schedule\ScheduleGeneratorInterface $plugin */
    $plugin = $this->generatorPluginManager->createInstance('default');
    return $this->pushSchedule($device, $plugin, FALSE, TRUE);
  }

}
