<?php

namespace Drupal\digital_signage_schedule;

use Drupal\media\Entity\MediaType;

class SequenceItem {

  private $type;
  private $entityId;
  private $entityType;
  private $entityBundle;
  private $duration;
  private $isDynamic;

  /**
   * SequenceItem constructor.
   *
   * @param int $contentEntityId
   * @param string $contentEntityType
   * @param string $contentEntityBundle
   * @param int $duration
   * @param bool $is_dynamic
   */
  public function __construct($contentEntityId, $contentEntityType, $contentEntityBundle, $duration, $is_dynamic) {
    $this->entityId = $contentEntityId;
    $this->entityType = $contentEntityType;
    $this->entityBundle = $contentEntityBundle;
    $this->duration = $duration;
    $this->isDynamic = $is_dynamic;
    $this->determineType();
  }

  /**
   * Determine content type.
   */
  protected function determineType(): void {
    $this->type = 'html';
    if ($this->entityType === 'media') {
      $mediaType = MediaType::load($this->entityBundle);
      if ($handler = $mediaType->getSource()->getSourceFieldDefinition($mediaType)->getSetting('handler')) {
        if (strpos($mediaType->getSource()->getPluginId(), 'video') === FALSE) {
          $this->type = 'image';
        }
        else {
          $this->type = 'video';
        }
      }
    }
  }

  /**
   * @return array
   */
  public function toArray(): array {
    return [
      'type' => $this->getType(),
      'entity' => [
        'type' => $this->getEntityType(),
        'id' => $this->getEntityId(),
      ],
      'duration' => $this->getDuration(),
      'dynamic' => $this->getIsDynamic(),
    ];
  }

  /**
   * @return mixed
   */
  public function getType() {
    return $this->type;
  }

  /**
   * Returns the content entity ID.
   *
   * @return int
   */
  public function getEntityId(): int {
    return $this->entityId;
  }

  /**
   * Returns the content entity type ID.
   *
   * @return string
   */
  public function getEntityType(): string {
    return $this->entityType;
  }

  /**
   * Returns the duration in seconds to display.
   *
   * @return int
   */
  public function getDuration(): int {
    return $this->duration;
  }

  /**
   * Returns TRUE if the content is dynamic or FALSE otherwise.
   *
   * @return bool
   */
  public function getIsDynamic(): bool {
    return $this->isDynamic;
  }

}
