<?php

namespace Drupal\digital_signage_framework\Controller;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Asset\LibraryDiscoveryInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Render\AttachmentsInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\digital_signage_framework\DigitalSignageFrameworkEvents;
use Drupal\digital_signage_framework\Event\CssFiles;
use Drupal\digital_signage_framework\Event\Rendered;
use Drupal\digital_signage_framework\Renderer;
use Drupal\digital_signage_device\Entity\Device;
use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;
use Endroid\QrCode\QrCode;
use Endroid\QrCodeBundle\Response\QrCodeResponse;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use InvalidArgumentException;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use function Jawira\PlantUml\encodep;
use Symfony\Component\DependencyInjection\ContainerInterface;

/** @noinspection PhpUnused */

/**
 * Provides the API controller.
 */
class Api implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * @var \Drupal\Core\Asset\LibraryDiscoveryInterface
   */
  protected $libraryDiscovery;

  /**
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * @var \Drupal\digital_signage_device\DeviceInterface
   */
  protected $device;

  /**
   * @var \Drupal\digital_signage_schedule\ScheduleInterface
   */
  protected $schedule;

  /**
   * @var \Drupal\digital_signage_platform\PlatformInterface
   */
  protected $platform;

  /**
   * @var \Drupal\digital_signage_framework\Renderer
   */
  protected $dsRenderer;

  /**
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * @var \Drupal\Core\Http\ClientFactory
   */
  protected $clientFactory;

  /**
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Api constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   * @param \Drupal\Core\Asset\LibraryDiscoveryInterface $library_discovery
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   * @param \Drupal\digital_signage_framework\Renderer $ds_renderer
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   * @param \Drupal\Core\Http\ClientFactory $client_factory
   * @param \Drupal\Core\Render\RendererInterface $renderer
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, ModuleHandlerInterface $module_handler, LibraryDiscoveryInterface $library_discovery, RequestStack $request_stack, Renderer $ds_renderer, EventDispatcherInterface $event_dispatcher, ClientFactory $client_factory, RendererInterface $renderer) {
    $this->config = $config_factory->get('digital_signage_framework.settings');
    $this->entityTypeManager = $entity_type_manager;
    $this->moduleHandler = $module_handler;
    $this->libraryDiscovery = $library_discovery;
    $this->request = $request_stack->getCurrentRequest();
    $this->dsRenderer = $ds_renderer;
    $this->eventDispatcher = $event_dispatcher;
    $this->clientFactory = $client_factory;
    $this->renderer = $renderer;

    if ($deviceId = $this->request->query->get('deviceId')) {
      $this->device = Device::load($deviceId);
      if ($this->device) {
        $this->platform = $this->device->getPlugin();
        $stored = $this->request->query->get('storedSchedule', 'true') === 'true';
        $this->schedule = $this->device->getSchedule($stored);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('module_handler'),
      $container->get('library.discovery'),
      $container->get('request_stack'),
      $container->get('digital_signage_framework.renderer'),
      $container->get('event_dispatcher'),
      $container->get('http_client_factory'),
      $container->get('renderer')
    );
  }

  /**
   * @return \Drupal\Core\Access\AccessResult
   */
  public function access(): AccessResult {
    switch ($this->request->query->get('mode')) {
      case 'load':
      /** @noinspection PhpMissingBreakStatementInspection */
      case 'preview':
        switch ($this->request->query->get('type', 'html')) {
          case 'css':
            break;

          case 'content':
            $contentPath = $this->request->query->get('contentPath');
            if (empty($contentPath)) {
              return AccessResult::forbidden('Missing content path.');
            }
            break;

          default:
            $entityType = $this->request->query->get('entityType');
            $entityId = (int) $this->request->query->get('entityId');
            if (empty($entityType) || empty($entityId)) {
              return AccessResult::forbidden('Missing entity defintion.');
            }
        }
        // Intentionally missing break statement.

      case 'schedule':
      case 'diagram':
      case 'screenshot':
      case 'log':
        if (empty($this->device)) {
          return AccessResult::forbidden('Missing or broken device definition.');
        }
        if (empty($this->schedule)) {
          return AccessResult::forbidden('Device has no schedule.');
        }
        if (isset($entityType, $entityId)) {
          foreach ($this->schedule->getItems() as $item) {
            if ($item['entity']['type'] === $entityType && $item['entity']['id'] === $entityId) {
              return AccessResult::allowed();
            }
          }
          return AccessResult::forbidden('Requested entity is not published on this device.');
        }
        return AccessResult::allowed();

      default:
        return AccessResult::forbidden('Missing or forbidden mode.');
    }
  }

  /**
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function request(): Response {
    switch ($this->request->query->get('mode')) {
      case 'diagram':
        $response = $this->diagram();
        break;

      case 'preview':
        if ($this->request->query->get('type', 'html') === 'html') {
          $response = $this->preview();
        }
        else {
          $response = $this->previewBinary();
        }
        break;

      case 'load':
        switch ($this->request->query->get('type', 'html')) {
          case 'html':
            $response = $this->load();
            break;

          case 'css':
            $response = $this->loadCSS();
            break;

          case 'content':
            $response = $this->loadContent();
            break;

          default:
            $response = $this->loadBinary();

        }
        break;

      case 'schedule':
        $response = $this->getSchedule();
        break;

      case 'screenshot':
        $response = $this->screenshot();
        break;

      case 'log':
        $response = $this->log();
        break;

      default:
        // This will never happen as we checked this in the above access callback.
        $response = new AjaxResponse();
    }

    $event = new Rendered($response, $this->device);
    $this->eventDispatcher->dispatch(DigitalSignageFrameworkEvents::RENDERED, $event);

    return $event->getResponse();
  }

  public function screenshot() {
    if ($screenshot = $this->device->getPlugin()->getScreenshot($this->device, (bool) $this->request->query->get('refresh'))) {
      $content = '<img src="' . $screenshot['uri'] . '" /><div class="screenshot-widget">' . $screenshot['takenAt'] . '</div>';
    }
    else {
      $content = '<p>' . $this->t('No screenshot available.') . '</p>';
    }
    $response = new AjaxResponse();
    $response->addCommand(new HtmlCommand('#digital-signage-preview .popup > .content', $content));
    return $response;
  }

  public function log() {
    switch ($this->request->query->get('type', 'debug')) {
      case 'error':
        $logs = $this->device->getPlugin()->showErrorLog($this->device);
        break;

      default:
        $logs = $this->device->getPlugin()->showDebugLog($this->device);
    }
    $rows = [];
    foreach ($logs as $item) {
      $rows[] = [
        'time' => $item['time'],
        'payload' => is_scalar($item['payload']) ? $item['payload'] : json_encode($item['payload'], JSON_PRETTY_PRINT),
      ];
    }
    $log = [
      '#type' => 'table',
      '#header' => [
        'time' => $this->t('Time'),
        'payload' => $this->t('Message'),
      ],
      '#rows' => $rows,
      '#empty' => $this->t('No log messages available.'),
      '#prefix' => '<div class="table-wrapper pre">',
      '#suffix' => '</div>',
    ];

    $response = new AjaxResponse();
    $response->addCommand(new HtmlCommand('#digital-signage-preview .popup > .content', $log));
    return $response;
  }

  /**
   * @return \Endroid\QrCodeBundle\Response\QrCodeResponse
   */
  public function qrcode(): QrCodeResponse {
    // TODO: Sanitize input.
    $url = $this->request->query->get('_link');
    $qrCode = new QrCode($url);

    return new QrCodeResponse($qrCode);
  }

  private function getSchedule(): JsonResponse {
    $items = $this->schedule->getItems();
    $content = [];
    foreach ($items as $key => $item) {
      if ($item['type'] === 'html') {
        $uid = $item['entity']['type'] . '_' . $item['entity']['id'];
        if (!isset($content[$uid])) {
          $elements = [
            '#theme' => 'digital_signage_framework',
            '#content' => $this->dsRenderer->buildEntityView(
              $item['entity']['type'],
              $item['entity']['id'],
              $this->device
            ),
            '#full_html' => FALSE,
          ];
          $content[$uid] = $this->dsRenderer->renderPlain($elements);
        }
        $items[$key]['content'] = $content[$uid];
      }
    }

    $response = new JsonResponse();
    $response->setData($items);
    return $response;
  }

  private function buildEntityView(): array {
    return $this->dsRenderer->buildEntityView(
      $this->request->query->get('entityType'),
      $this->request->query->get('entityId'),
      $this->device
    );
  }

  /**
   * @return \Drupal\Core\Render\AttachmentsInterface
   */
  private function load(): AttachmentsInterface {
    $output = [
      '#theme' => 'digital_signage_framework',
      '#content' => $this->buildEntityView(),
    ];
    return $this->dsRenderer->buildHtmlResponse($output);
  }

  /**
   * @return string
   */
  private function getFileUri(): string {
    /** @var \Drupal\media\MediaInterface $media */
    /** @var \Drupal\file\FileInterface $file */
    if (($media = Media::load($this->request->query->get('entityId'))) && $file = File::load($media->getSource()
        ->getSourceFieldValue($media))) {
      $file_uri = $file->getFileUri();
      try {
        /** @var \Drupal\image\ImageStyleInterface $image_style */
        if (($media->bundle() === 'image') && $image_style = $this->entityTypeManager->getStorage('image_style')
            ->load('digital_signage_' . $this->device->getOrientation())) {
          $derivative_uri = $image_style->buildUri($file_uri);
          if (file_exists($derivative_uri) || $image_style->createDerivative($file_uri, $derivative_uri)) {
            $file_uri = $derivative_uri;
          }
        }
      } catch (InvalidPluginDefinitionException $e) {
      } catch (PluginNotFoundException $e) {
      }
      return $file_uri;
    }
    throw new InvalidArgumentException('File not found.');
  }

  /**
   * @return string
   */
  private function prepareCSS(): string {
    $event = new CssFiles($this->device);
    if ($this->moduleHandler->moduleExists('layout_builder')) {
      foreach ($this->libraryDiscovery->getLibrariesByExtension('layout_builder') as $library) {
        foreach ($library['css'] as $css) {
          $event->addFile($css['data']);
        }
      }
    }
    $event->addFile(drupal_get_path('module', 'digital_signage_framework') . '/css/digital-signage.css');
    $this->eventDispatcher->dispatch(DigitalSignageFrameworkEvents::CSSFILES, $event);
    foreach (explode(PHP_EOL, $this->config->get('css')) as $file) {
      $event->addFile($file);
    }

    $css = '';
    foreach ($event->getFiles() as $css_file) {
      if (!empty($css_file) && file_exists($css_file)) {
        $css .= file_get_contents($css_file) . PHP_EOL;
      }
    }
    return $css;
  }

  /**
   * @return \Symfony\Component\HttpFoundation\Response
   */
  private function loadCSS(): Response {
    $css = $this->prepareCSS();
    $headers = [
      'Content-Type' => 'text/css',
      'Content-Length' => strlen($css),
    ];
    return new Response($css, 200, $headers);
  }

  /**
   * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
   */
  private function loadContent(): BinaryFileResponse {
    $content_path = Url::fromUserInput('/', [
      'absolute' => TRUE,
    ])->toString() . substr(base64_decode($this->request->query->get('contentPath')), 1);
    $file_uri = 'temporary://content-' . hash('md5', $content_path);
    try {
      $client = $this->clientFactory->fromOptions(['base_uri' => $content_path]);
      $options = [];
      if ($authorization = $this->request->headers->get('authorization')) {
        $options[RequestOptions::HEADERS]['Authorization'] = is_array($authorization) ?
          reset($authorization) :
          $authorization;
      }
      $response = $client->request('get', NULL, $options);
      file_put_contents($file_uri, $response->getBody()->getContents());
    } catch (GuzzleException $e) {
      file_put_contents($file_uri, '');
    }

    $headers = [
      'Content-Type' => mime_content_type($file_uri),
      'Content-Length' => filesize($file_uri),
    ];
    return new BinaryFileResponse($file_uri, 200, $headers);
  }

  /**
   * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
   */
  private function loadBinary(): BinaryFileResponse {
    $file_uri = $this->getFileUri();
    $headers = [
      'Content-Type' => mime_content_type($file_uri),
      'Content-Length' => filesize($file_uri),
    ];
    return new BinaryFileResponse($file_uri, 200, $headers);
  }

  /**
   * @param array|string $output
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  private function popupContent($output): AjaxResponse {
    $css = '<style>' . $this->prepareCSS() . '</style>';
    $content = is_array($output) ? $this->renderer->renderRoot($output) : $output;
    $content = '<iframe srcdoc="' . htmlspecialchars($css . $content) . '"></iframe>';

    $response = new AjaxResponse();
    $response->addCommand(new HtmlCommand('#digital-signage-preview .popup > .content', $content));
    return $response;
  }

  /**
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  private function preview(): AjaxResponse {
    return $this->popupContent($this->buildEntityView());
  }

  private function previewBinary(): AjaxResponse {
    $file_uri = file_create_url($this->getFileUri());
    switch ($this->request->query->get('type')) {
      case 'image':
        $output = '<img src="' . $file_uri . '" alt="" />';
        break;

      case 'video':
        $output = '<video src="' . $file_uri . '" autoplay="autoplay" />';
        break;

      default:
        $output = 'Problem loading media';
    }
    return $this->popupContent($output);
  }

  /**
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  private function diagram(): AjaxResponse {
    $items = $this->schedule->getItems();
    switch ($this->request->query->get('umlType')) {
      case 'activity':
        $uml = $this->umlActivity($items);
        break;

      case 'sequence':
      default:
        $uml = $this->umlSequence($items);
    }

    try {
      $client = $this->clientFactory->fromOptions(['base_uri' => $this->config->get('plantuml_url') . '/svg/' . encodep($uml)]);
      $response = $client->request('get', NULL);
      $output = $response->getBody()->getContents();
    }
    catch (GuzzleException $e) {
      $output = '';
    }
    catch (Exception $e) {
      $output = 'Problem encoding UML: <br><pre>' . $uml . '</pre>';
    }
    $response = new AjaxResponse();
    $response->addCommand(new HtmlCommand('#digital-signage-preview .popup > .content', $output));
    return $response;
  }

  /**
   * @param string $type
   * @param int $id
   *
   * @return string
   */
  private function getEntityLabel($type, $id): string {
    try {
      /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
      $entity = $this->entityTypeManager->getStorage($type)->load($id);
      if ($entity !== NULL) {
        return $entity->label();
      }
    } catch (InvalidPluginDefinitionException $e) {
    } catch (PluginNotFoundException $e) {
    }
    return 'N/A';
  }

  /**
   * @param array $items
   *
   * @return string
   */
  private function umlActivity($items): string {
    $uml = '@startuml' . PHP_EOL . 'while (Loop)' . PHP_EOL;
    foreach ($items as $item) {
      $uml .= strtr(':**%label**%EOL%type: **%entityType/%entityId**%EOL%duration seconds%dynamic;%EOL', [
        '%EOL' => PHP_EOL,
        '%label' => $this->getEntityLabel($item['entity']['type'], $item['entity']['id']),
        '%type' => $item['type'],
        '%entityType' => $item['entity']['type'],
        '%entityId' => $item['entity']['id'],
        '%duration' => $item['duration'],
        '%dynamic' => $item['dynamic'] ? '/dynamic' : '',
      ]);
    }
    $uml .= 'endwhile' . PHP_EOL . 'stop' . PHP_EOL . '@enduml';
    return $uml;
  }

  /**
   * @param array $items
   *
   * @return string
   */
  private function umlSequence($items): string {
    $uml = '@startuml' . PHP_EOL;
    foreach ($items as $item) {
      $uml .= strtr('participant "%label" as %id << %type: %path >>%EOL', [
        '%EOL' => PHP_EOL,
        '%label' => $this->getEntityLabel($item['entity']['type'], $item['entity']['id']),
        '%type' => $item['type'],
        '%path' => $item['entity']['type'] . '/' . $item['entity']['id'],
        '%id' => $item['entity']['type'] . '_' . $item['entity']['id'],
      ]);
    }
    $previous = FALSE;
    foreach ($items as $item) {
      $current = $item['entity']['type'] . '_' . $item['entity']['id'];
      if ($previous) {
        $uml .= strtr('%previous --> %id: %duration seconds%EOL', [
          '%EOL' => PHP_EOL,
          '%previous' => $previous,
          '%id' => $current,
          '%duration' => $item['duration'],
        ]);
      }
      $previous = $current;
    }
    $uml .= '@enduml';
    return $uml;
  }

}
